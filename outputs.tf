#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
output "JS_FUNCTION_NAMES" {
    value = module.lambda.js_lambda_hello_world_name  
}

output "S3_FUNCTION_NAME" {
    value = module.lambda.s3_lambda_hello_world_name  
}

output "HELLO_BASE_URL" {
    value = module.api-gateways.hello_base_url
}