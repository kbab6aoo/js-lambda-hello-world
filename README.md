# Nodejs Lambda

Configuration in this directory will create the following resources:

* Build a deployment package   
* Create a zip for the package
* Upload the zip file to an S3 Bucket
* Configure Terraform
* Build the Lambda in AWS

# Usage
To use this repositoy upload or fork the contents of this repository and run the pipeline.

You will need an AWS account and access to GitLab

```
aws lambda invoke --region=eu-west-1 --function-name=js-lambda-hello-world response.json

cat response.json

curl "https://<id>.execute-api.us-east-1.amazonaws.com/dev/hello?Name=Yomi"

curl -X POST \
-H "Content-Type: application/json" \
-d '{"name":"Yomi"}' \
"https://<id>.execute-api.us-east-1.amazonaws.com/dev/hello"

aws lambda invoke \
--region=us-east-1 \
--function-name=s3 \
--cli-binary-format raw-in-base64-out \
--payload '{"bucket":"test-<your>-<name>","object":"hello.json"}' \
response.json
```
