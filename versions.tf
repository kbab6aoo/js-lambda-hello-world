#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://jagho.info
# Phone: +44 (0) 75 3331 8069
#################################################
terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 4.51.0"
      }
  }
  required_version = ">= 1.2.9"
}
