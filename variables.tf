#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################

###############################################################################
# Expected values from the caller
###############################################################################
variable account_id {}
variable asset_id {}
variable aws_region {}
variable env_type {}
variable hello_zip_file {}
variable s3_zip_file {}