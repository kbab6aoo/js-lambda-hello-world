#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://jagho.info
# Phone: +44 (0) 75 3331 8069
#################################################
variables:
  TF_AWS_DEFAULT_REGION: "eu-west-1"
  CI_ROLE_NAME:          "GitLabCI-OIDC"
  DEPLOYMENT_ROLE:       "arn:aws:iam::${SBX_DEVOPS_ACCOUNT_ID}:role/ci/${CI_ROLE_NAME}"
  PATH_HELLO_APP:        "$CI_PROJECT_DIR/hello"
  PATH_S3_APP:           "$CI_PROJECT_DIR/s3"
  ASSET_ID:              "2023"
  ENV_TYPE:              "dev"
  TF_KEY:                "js-lambda-hello-world.v0/tfstate"
  TF_LOCKS:              "db_js-lambda-hello-world"

.hello_variables: &hello_variables
  APP_DIR: "${PATH_HELLO_APP}"

.s3_variables: &s3_variables
  APP_DIR: "${PATH_S3_APP}"

stages:
  - build
  - archive
  - test
  - upload-to-s3
  - validate
  - plan
  - deploy

.alpine_deploy_script: &alpine_deploy_script
    before_script:
    - cd ${APP_DIR}
    - npm install --progress=false

.ubuntu_deploy_script: &ubuntu_deploy_script
    before_script:
    - apt-get update
    - apt-get install zip -y
    - mkdir build

.deploy_script: &deploy_script
  before_script:
    - echo "===== assuming permissions => ${DEPLOYMENT_ROLE} ====="
    - STS_TOKEN=($(aws sts assume-role-with-web-identity
      --role-arn ${DEPLOYMENT_ROLE}
      --role-session-name "gitlab-${CI_PROJECT_ID}-${CI_PIPELINE_ID}"
      --web-identity-token ${CI_JOB_JWT_V2}
      --duration-seconds 3600
      --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]'
      --output text))
    - export AWS_DEFAULT_REGION="${TF_AWS_DEFAULT_REGION}"     
    - export AWS_ACCESS_KEY_ID="${STS_TOKEN[0]}"
    - export AWS_SECRET_ACCESS_KEY="${STS_TOKEN[1]}"
    - export AWS_SESSION_TOKEN="${STS_TOKEN[2]}" 
    - aws sts get-caller-identity
    - terraform init 
      -backend-config "bucket=${SBX_DEVOPS_ACCOUNT_ID}-${ASSET_ID}-${ENV_TYPE}-tfstate-${TF_AWS_DEFAULT_REGION}"
      -backend-config "key=${ENV_TYPE}/${TF_KEY}"
      -backend-config "region=${TF_AWS_DEFAULT_REGION}"
      -backend-config "dynamodb_table=${SBX_DEVOPS_ACCOUNT_ID}-${ASSET_ID}-${ENV_TYPE}-tf-${TF_LOCKS}-lock-${TF_AWS_DEFAULT_REGION}"

.sbx01_variables: &sbx01_variables
  ACCOUNT_ID: "${SBX_DEVOPS_ACCOUNT_ID}"
  REGION: "${TF_AWS_DEFAULT_REGION}"
  DEPLOYMENT_ROLE: "arn:aws:iam::${SBX_DEVOPS_ACCOUNT_ID}:role/ci/${CI_ROLE_NAME}"
  TF_ENVIRONMENT: "${DEV_ENV}"

.base-alpine:
  image: 
    name: node:18-alpine

.base-ubuntu:
  image: 
    name: ubuntu:23.04

.base-tf-awscli-azcli:
  image:
    name: public.ecr.aws/i0s3q1w9/jagho-tf-awscli-azcli:latest
    entrypoint: [""]

build hello app:
  stage: build
  <<: *alpine_deploy_script
  extends: .base-alpine
  variables:
    <<: *hello_variables
  script:
    - npm install aws-sdk
  artifacts:
      untracked: false
      when: on_success
      expire_in: "1 hour"
      paths:
        - hello

test-hello-app:
  stage: test
  <<: *alpine_deploy_script
  extends: .base-alpine
  dependencies:
    - build hello app
  variables:
    <<: *hello_variables
  script:
    - npm run deploy

archive-hello:
  stage: archive
  <<: *ubuntu_deploy_script
  extends: .base-ubuntu
  only:
    - develop
  when:
    on_success
  script:
    - zip -g -j build/hello.zip hello/index.js
  artifacts:
      paths:
      - build/hello.zip

###################################################################
build s3 app:
  stage: build
  <<: *alpine_deploy_script
  extends: .base-alpine
  variables:
    <<: *s3_variables
  script:
    - npm install aws-sdk
  artifacts:
      untracked: false
      when: on_success
      expire_in: "1 hour"
      paths:
        - s3

archive-s3:
  stage: archive
  <<: *ubuntu_deploy_script
  extends: .base-ubuntu
  only:
    - develop
  when:
    on_success
  script:
    - zip -g -j build/s3.zip s3/function.js
  artifacts:
      paths:
      - build/s3.zip

upload-artifacts:
  stage: upload-to-s3
  <<: *deploy_script
  extends: .base-tf-awscli-azcli
  dependencies:
    - archive-hello
    - archive-s3
  script: 
    - aws --version
    - aws s3 sync --delete build s3://$TF_PAYLOAD_BUCKET
  only:
    - develop

validate-terraform configuration:
  stage: validate
  <<: *deploy_script
  extends: .base-tf-awscli-azcli
  script:
    - terraform version
    - pwd
    - terraform init -backend=false
    - echo "===== validating terraform code =============="
    - terraform validate
    - terraform fmt -list=true -write=false -recursive

##########################################################
# SBX01 DevOps Sandbox Account
##########################################################
build-terraform-infrastructure:
  stage: plan
  <<: *deploy_script
  extends: .base-tf-awscli-azcli
  variables:
    <<: *sbx01_variables
  script: 
    - pwd
    - ls -l
    - aws s3 ls
    - terraform plan
      -var account_id="${SBX_DEVOPS_ACCOUNT_ID}"
  only:
    refs:
      - develop

deploy-terraform-infrastructure:
  stage: deploy
  <<: *deploy_script
  extends: .base-tf-awscli-azcli
  variables:
    <<: *sbx01_variables
  script: 
    - pwd
    - ls -l
    - aws s3 ls
    - terraform apply -auto-approve
      -var account_id="${SBX_DEVOPS_ACCOUNT_ID}"
    - terraform fmt -list=true -write=false -recursive
  only:
    refs:
      - develop
