#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
terraform {
  backend "s3" {
    encrypt = true
  }
}

module "lambda" {
    source = "./modules/lambda"

    account_id          = var.account_id
    asset_id            = var.asset_id
    aws_region          = var.aws_region
    env_type            = var.env_type
    hello_zip_file      = var.hello_zip_file
    s3_zip_file         = var.s3_zip_file
}

module "api-gateways" {
  source = "./modules/api-gateway"

  depends_on = [
    module.lambda
  ]
}
