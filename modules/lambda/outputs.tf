#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
output "js_lambda_hello_world_name" {
    value = aws_lambda_function.js_lambda_hello_world.function_name
}

output "s3_lambda_hello_world_name" {
    value = aws_lambda_function.s3_lambda_hello_world.function_name
}