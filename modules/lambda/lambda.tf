#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
resource "aws_iam_role" "js_hello_lambda_exec" {
    name = var.js_aws_iam_role

    assume_role_policy = templatefile("./templates/lambda-assume-role-policy.tpl",{})
}

resource "aws_iam_role_policy_attachment" "js_lambda_hello_world_policy" {
    role = aws_iam_role.js_hello_lambda_exec.name
    policy_arn = var.aws_iam_role_policy_attachment
}

resource "aws_lambda_function" "js_lambda_hello_world" {
    function_name   = var.js_lambda_function
    description     = var.description

    s3_bucket           = data.aws_s3_bucket.gitlab_lambda_artifacts.id
    s3_key              = data.aws_s3_object.js_lambda_hello_world.key
    s3_object_version   = data.aws_s3_object.js_lambda_hello_world.version_id    
    runtime             = var.function_runtime
    handler             = var.index_handler

    role = aws_iam_role.js_hello_lambda_exec.arn
}

resource "aws_cloudwatch_log_group" "js_lambda_hello_world" {
    name = "/aws/lambda/${aws_lambda_function.js_lambda_hello_world.function_name}"

    retention_in_days = var.retention_in_days
}

###############################################################################
# S3 Lambda
resource "aws_iam_role" "s3_hello_lambda_exec" {
    name = var.s3_aws_iam_role

    assume_role_policy = templatefile("./templates/lambda-assume-role-policy.tpl",{})
}

resource "aws_iam_role_policy_attachment" "s3_lambda_hello_world_policy" {
    role = aws_iam_role.s3_hello_lambda_exec.name
    policy_arn = var.aws_iam_role_policy_attachment
}

resource "aws_iam_policy" "s3_lambda_artifacts_bucket_access" {
    name = "s3LambdaArtifactsS3BucketAccess"
    description = "Grants permission to retrieve objects from Amazon S3.  This policy was created with Terraform"
    # policy = templatefile("./templates/s3-lambda-artifacts-access-policy.tpl",{})
    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
        {
            Action = [
            "s3:GetObject",
            ]
            Effect   = "Allow"
            Resource = "arn:aws:s3:::${data.aws_s3_bucket.s3_lambda_artifacts.id}/*"
        },
        ]
    })
}


resource "aws_iam_role_policy_attachment" "s3_lambda_artifacts_bucket_access" {
    role = aws_iam_role.s3_hello_lambda_exec.name
    policy_arn = aws_iam_policy.s3_lambda_artifacts_bucket_access.arn
}

resource "aws_lambda_function" "s3_lambda_hello_world" {
    function_name   = var.s3_lambda_function
    description     = var.description

    s3_bucket           = data.aws_s3_bucket.gitlab_lambda_artifacts.id
    s3_key              = data.aws_s3_object.s3_lambda_hello_world.key
    s3_object_version   = data.aws_s3_object.s3_lambda_hello_world.version_id    
    runtime             = var.function_runtime
    handler             = var.function_handler

    role = aws_iam_role.s3_hello_lambda_exec.arn
}

resource "aws_cloudwatch_log_group" "s3_lambda_hello_world" {
    name = "/aws/lambda/${aws_lambda_function.s3_lambda_hello_world.function_name}"

    retention_in_days = var.retention_in_days
}
