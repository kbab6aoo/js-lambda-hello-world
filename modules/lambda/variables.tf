#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
variable js_aws_iam_role {
    type = string
    default = "js-lambda-hello-world"
}

variable s3_aws_iam_role {
    type = string
    default = "s3-lambda-hello-world"
}

variable aws_iam_role_policy_attachment {
  type = string
  default = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

variable description {
    type = string
    default = "This resource is being managed by terraform. Please do not DELETE!..."
}

variable js_lambda_function {
    type = string
    default = "js-lambda-hello-world"
}

variable s3_lambda_function {
    type = string
    default = "s3-lambda-hello-world"
}

variable function_runtime {
    type = string
    default = "nodejs16.x"
}

variable index_handler {
    type = string
    default = "index.handler"
}

variable function_handler {
    type = string
    default = "function.handler"
}

variable "hello_zip_file" {
    type = string
    default = "hello.zip"
}

variable "s3_zip_file" {
    type = string
    default = "s3.zip"
}

variable retention_in_days {
    type = number
    default = 7
}

############################################################
#  Global Variables
############################################################
variable "account_id" {
    type = number
}

variable "asset_id" {
    type = number
}

variable "aws_region" {
    type = string
}

variable "env_type" {
    type = string
}
