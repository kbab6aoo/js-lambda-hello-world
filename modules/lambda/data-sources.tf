#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
data "aws_s3_bucket" "gitlab_lambda_artifacts" {  
  bucket = "${var.account_id}-${var.asset_id}-${var.env_type}-gitlab-lambda-artifacts-${var.aws_region}"
}

data "aws_s3_bucket" "s3_lambda_artifacts" {  
  bucket = "${var.account_id}-${var.asset_id}-${var.env_type}-s3-lambda-artifacts-${var.aws_region}"
}


data "aws_s3_object" "js_lambda_hello_world" {
  bucket = "${var.account_id}-${var.asset_id}-${var.env_type}-gitlab-lambda-artifacts-${var.aws_region}"
  key    = "js-lambda-hello-world/${var.hello_zip_file}"
}

data "aws_iam_role" "s3_lambda_hello_world_exec" {
  name = "js-lambda-hello-world"

  depends_on = [
    aws_lambda_function.js_lambda_hello_world
  ]
}

data "aws_s3_object" "s3_lambda_hello_world" {
  bucket = "${var.account_id}-${var.asset_id}-${var.env_type}-gitlab-lambda-artifacts-${var.aws_region}"
  key    = "js-lambda-hello-world/${var.s3_zip_file}"
}