#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
output "hello_base_url" {
    value = aws_apigatewayv2_stage.dev.invoke_url
}