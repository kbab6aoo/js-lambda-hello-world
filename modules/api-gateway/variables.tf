#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
variable "function_name" {
  type = string
  default = "js-lambda-hello-world"
}

variable "retention_in_days" {
    type = number
    default = 7
}
