#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
resource "aws_apigatewayv2_api" "this" {
    name = "js-lambda-hello-world"
    protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "dev" {
    api_id = aws_apigatewayv2_api.this.id

    name = "dev"
    auto_deploy = true

    access_log_settings {
      destination_arn = aws_cloudwatch_log_group.this_api_gw.arn

      format = jsonencode({
        requestId                   = "$context.requestId"
        sourceIp                    = "$context.identity.sourceIp"
        requestTime                 = "$context.requestTime"
        protocol                    = "$context.protocol"
        httpMethod                  = "$context.httpMethod"
        resourcePath                = "$context.resourcePath"
        routeKey                    = "$context.routeKey"
        status                      = "$context.status"
        responseLength              = "$context.responseLength"
        integrationErrorMessage     = "$context.integrationErrorMessage"
      })
    }
}

resource "aws_cloudwatch_log_group" "this_api_gw" {
  name = "/aws/api-gw/${aws_apigatewayv2_api.this.name}"

  retention_in_days = var.retention_in_days
}
