#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
resource "aws_apigatewayv2_integration" "hello" {
    api_id                = aws_apigatewayv2_api.this.id

    integration_uri       = data.aws_lambda_function.js_lambda_hello_world.invoke_arn
    integration_type      = "AWS_PROXY"
    integration_method    = "POST"
}

resource "aws_apigatewayv2_route" "get_hello" {
    api_id          = aws_apigatewayv2_api.this.id

    route_key       = "GET /hello"
    target          = "integrations/${aws_apigatewayv2_integration.hello.id}"
}

resource "aws_apigatewayv2_route" "post_hello" {
    api_id          = aws_apigatewayv2_api.this.id

    route_key       = "POST /hello"
    target          = "integrations/${aws_apigatewayv2_integration.hello.id}"
}

resource "aws_lambda_permission" "api_gw" {
    statement_id    = "AllowExecutionFromAPIGateway"
    action          = "lambda:invokeFunction"
    function_name   = data.aws_lambda_function.js_lambda_hello_world.function_name
    principal       = "apigateway.amazonaws.com"

    source_arn      = "${aws_apigatewayv2_api.this.execution_arn}/*/*"
}
