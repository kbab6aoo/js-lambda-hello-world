#################################################
# Email: Yomi.Ogunyinka@JaghoConsultants.co.uk
# WebSite: https://www.jagho.tk
# Phone: +44 (0) 75 3331 8069
#################################################
data "aws_lambda_function" "js_lambda_hello_world" {
  function_name = var.function_name
}